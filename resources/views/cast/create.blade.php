@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">

    @csrf
    <div class="form-group">
      <label for="input nama">Nama</label>
      <input name="nama" type="text" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputEmail1">Umur</label>
        <input name="umur" type="text" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputEmail1">Biodata</label><br>
        <textarea name="bio" cols="140" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection