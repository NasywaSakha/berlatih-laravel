<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        # code...
        $cast = DB:: table('cast')-> get(); //nama table
        return view ('cast.index', compact('cast')); //nama folder, //nama variabel
    }

    public function create()// view, cast
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request -> validate([ //validasi kolom di tabel di file create
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB :: table ('cast') -> insert ([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        return redirect('/cast'); //route get
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.show', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast= DB::table('cast')->find($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        $request -> validate([ //validasi kolom di tabel di file create
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->nama,
                    'umur' => $request->umur,
                    'bio' => $request->biodata,
                ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
       DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}