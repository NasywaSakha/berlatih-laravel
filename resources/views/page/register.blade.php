<!DOCTYPE html>
<html>
  <body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>

    <form action="/welcome" method="post">
      @csrf
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br />

      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" /><br /><br />

      <label>Gender:</label>
      <br /><br />
      <input type="radio" name="suggest" value="definitely" /><label>Male</label><br />
      <input type="radio" name="suggest" value="Maybe" /><label>Female</label><br />
      <input type="radio" name="suggest" value="" Not sure /><label>Other</label> <br /><br />
      <label>Nationality:</label>
      <br /><br />

      <select>
        <option>Indonesia</option>
        <option>Singapore</option>
        <option>Malaysia</option>
        <option>Australia</option>
      </select>
      <br /><br />

      <label>Language spoken:</label><br /><br />
      <input type="checkbox" name="Indonesia" value="1" />
      <label for="vehicle1">Bahasa Indonesia</label><br />
      <input type="checkbox" name="English" value="2" />
      <label for="vehicle2">English</label><br />
      <input type="checkbox" name="Other" value="3" />
      <label for="vehicle3">Other</label>
      <br /><br />

      <label>Bio:</label><br /><br />
      <textarea name="message" rows="10" cols="30"></textarea>
      <br /><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
