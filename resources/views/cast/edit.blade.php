@extends('layouts.master')

@section('judul')
    Halaman Edit Cast
@endsection

@section('content')
<a href="/cast" class="btn btn-primary mb-2">Kembali</a>
<form action="/cast/{{$cast->id}}" method="POST">

    @csrf
     @method('put') {{--sesuai route --}}

    <div class="form-group">
      <label for="input nama">Nama</label>
      <input type="hidden" name="_method" value="PUT">
      <input name="nama" value="{{$cast->nama}}" type="text" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputEmail1">Umur</label>
        <input name="umur" value="{{$cast->umur}}" type="text" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputEmail1">Biodata</label><br>
        <textarea name="bio" cols="140" rows="10">value="{{$cast->bio}}"</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection