<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Tugas 12 – Membuat Web Statis dengan Laravel
// by Rezky putra akkif
 
// Target Pembelajaran
 

// Berkenalan dengan struktur MVC (Bagian Controller)
// Mengerti Routing Laravel
// Mengerti Penggunaan Controller di Laravel
// Handle Request dari URL dan form
 

// Petunjuk Pengerjaan
 

// Masih ingat dengan challenge hari pertama materi HTML? hari ini kita akan pakai halaman tersebut di project laravel. Kamu diminta untuk membuat sebuah project Laravel. Di dalam project tersebut terdapat 3 route yaitu: Home, Register, dan Welcome.

// Langkah-langkahnya adalah:

 

// Buatlah route terlebih dahulu untuk setiap halaman yang ingin dibuat.
// Buat halaman blade untuk masing-masing route yang ingin dibuat. simpanlah di folder resources/views
// Buat dua controller : HomeController dan AuthController.
// Route Home diatur oleh HomeController, sedangkan route Register dan Welcome diatur oleh AuthController.
// Alur program: dimulai dari halaman Home(route: '/') terdapat link menuju route register (route: '/register') . di halaman register terdapat form untuk mengisi nama depan, nama belakang, dan isian lainnya. Ketika disubmit di halaman register, form diarahkan ke halaman welcome ( route : '/welcome') dengan membawa data nama depan dan nama belakang yang akan ditampilkan di halaman welcome tersebut.

Route::get('/home', 'HomeController@utama');
Route::get('/register', 'AuthController@regis');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/', function(){
return view ('layouts.master');
});

Route::get('/data-tables', function(){//route yang ada di url
    return view('page.data-table'); //manggil view folder.file
});

Route::get('/table', function(){
    return view('page.table');
});

//CRUD Cast

//read data cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast-id}', 'CastController@show'); //get untuk ambil data

//create data cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store'); //post untuk input 

//Update data Cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update'); //put untuk update

//Delete data Cast
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//method nya put brrti root nya put, disesuaikan dengan route nya 